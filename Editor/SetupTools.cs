#if UNITY_EDITOR
using System.Threading.Tasks;
using System.Net.Http;
using System.IO;
using UnityEngine;
using UnityEditor;


namespace Novasloth {
    public static class SetupTools {

        ///////////////////////////////////////////////////////////////////
        // G L O B A L   V A R I A B L E S
        ///////////////////////////////////////////////////////////////////

        private const string MENU_SETUP_PATH = "Novasloth/Tools/Setup/";

        ///////////////////////////////////////////////////////////////////
        // S E T U P   T O O L   M E N U
        ///////////////////////////////////////////////////////////////////

        // Call all setup methods
        [MenuItem(MENU_SETUP_PATH + "Setup Everything!")]
        public static void SetupEverything () {
            SetupDefaultFolders();
            SetupSceneObjects();
            SetupGitignoreFile();
        }

        // Setup folders utilized in Novasloth Games projects
        [MenuItem(MENU_SETUP_PATH + "Setup Default Folders")]
        public static void SetupDefaultFolders () {
            CreateFolders(new string[] {
                "Editor",
                "Models",
                "Materials",
                "Prefabs",
                "Scripts"
            });
            AssetDatabase.Refresh();
            Debug.Log("INFO :: SetupTools :: Default folders created!");
        }

        // Create empty objects in current scene for organization
        [MenuItem(MENU_SETUP_PATH + "Setup Scene Objects")]
        public static void SetupSceneObjects () {
            CreateGameObjects(
                "SYSTEMS",
                "CAMERAS",
                "GUI",
                "LIGHTS",
                "OBJECTS",
                ""
            );
            Debug.Log("INFO :: SetupTools :: Organized scene objects created!");
        }

        // Setup .gitignore file for the project
        [MenuItem(MENU_SETUP_PATH + "Setup .gitignore File")]
        public static async void SetupGitignoreFile () {
            string ignoreFileContents = await DownloadFileContents(GetGistFileURL("24d6d17b3bdfa882633bbd4df326937e"));
            File.WriteAllText(Path.GetFullPath(".gitignore"), ignoreFileContents);
            Debug.Log("INFO :: SetupTools :: .gitignore file created in root directory!");
        }

        ///////////////////////////////////////////////////////////////////
        // H E L P E R   M E T H O D S
        ///////////////////////////////////////////////////////////////////

        // Create folders under a specified root folder
        public static void CreateFolders (string rootDirectory, params string[] folders) {
            string fullPath = Path.Combine(Application.dataPath, rootDirectory);

            foreach (string folder in folders) {
                Directory.CreateDirectory(
                    Path.Combine(fullPath, folder)
                );
            }
        }

        // Create folders under the "Assets" folder
        public static void CreateFolders (params string[] folders) {
            foreach (string folder in folders) {
                Directory.CreateDirectory(
                    Path.Combine(Application.dataPath, folder)
                );
            }
        }

        // Get Gitlab snippet file by project name and snippet id
        public static string GetGistFileURL (string gistId)
            => $"https://gist.githubusercontent.com/novasloth-games/{gistId}/raw";

        // HTTP GET request to github for gist file content
        public static async Task<string> DownloadFileContents (string url) {
            using HttpClient client = new HttpClient();
            HttpResponseMessage res = await client.GetAsync(url);

            return await res.Content.ReadAsStringAsync();
        }

        // Create empty gameobjects in current scene
        public static void CreateGameObjects (params string[] names) {
            foreach (string name in names) {
                GameObject gameObject = new GameObject($"//{name}");
                gameObject.transform.position = Vector3.zero;
            }
        }
    }
}
#endif